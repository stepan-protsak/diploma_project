$(document).ready(function(){

	$("#select1").on("change", function(){
        var $target = $("#select2").val("0"),
            gender = $(this).val();
        
        $target
            .toggleClass("hidden", gender === "")
            .find("option:gt(0)").addClass("hidden")
        	.siblings().filter("[data-categories-id="+gender+"]").removeClass("hidden"); 
    });

	$("#select2").on("change", function(){
        var $target = $("#select3").val("0"),
            gender = $(this).val();
        
        $target
            .toggleClass("hidden", gender === "")
            .find("option:gt(0)").addClass("hidden")
        	.siblings().filter("[data-sub-categories-id="+gender+"]").removeClass("hidden"); 
    });


});
