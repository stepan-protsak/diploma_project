﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_category_databases extends CI_Migration {

	public function up()
	{

		//creating main menu table
		$this->db->query('
			CREATE TABLE categories
			(
			id int NOT NULL AUTO_INCREMENT,
			name varchar(255) NOT NULL,
			PRIMARY KEY (ID)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;
			');

		$this->db->query("
			INSERT INTO categories 
			(name)
			VALUES 
			('Давачі'),
			('Нормалізатори'),
			('Регулятори'),
			('PLC'),
			('Виконавчі пристрої'),
			('Компютери'),
			('Мережеві засоби'),
			('Панелі оператора'),
			('Пневматика'),
			('Засоби візуалізації та керування'),
			('Щити, пульти, штативи');
			");

		//creating table for sub categories
		$this->db->query('
			CREATE TABLE sub_categories
			(
			id int NOT NULL AUTO_INCREMENT,
			name varchar(255) NOT NULL,
			categories_id int NOT NULL,
			PRIMARY KEY (ID),
			FOREIGN KEY (categories_id) REFERENCES categories(id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;
			');

		$this->db->query("
			INSERT INTO sub_categories 
			(name, categories_id)
			VALUES 
			('Тиск', 1),
			('Температура', 1),
			('Витрата', 1),
			('Рівень', 1),
			('Якість', 1),
			('Для термопар', 2),
			('Для термоопрів', 2),
			('Електродвигунні', 5),
			('Соленоїдні', 5),
			('Додаткові блоки', 5),
			('Енергопостачалтьна', 9),
			('Направляюча - Регулююча', 9),
			('Технічні індикатори', 10),
			('Реєстратори', 10),
			('Блоки ручного керування', 10);
			");
		
		//creating table for sub sub categories
			$this->db->query('
			CREATE TABLE sub_sub_categories
			(
			id int NOT NULL AUTO_INCREMENT,
			name varchar(255) NOT NULL,
			sub_categories_id int NOT NULL,
			PRIMARY KEY (ID),
			FOREIGN KEY (sub_categories_id) REFERENCES sub_categories(id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;
			');

			$this->db->query("
			INSERT INTO sub_sub_categories 
			(name, sub_categories_id)
			VALUES
			('Частотні перетворювачі', 10),
			('Пускові пристрої', 10),
			('Перетворювачі', 10),
			('Позиціонери', 10);
			");
	}

	public function down()
	{
		$this->db->query('DROP TABLE sub_sub_categories');
		$this->db->query('DROP TABLE sub_categories');
		$this->db->query('DROP TABLE categories');
	}

}
