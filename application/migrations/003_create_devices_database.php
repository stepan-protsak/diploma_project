<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_devices_database extends CI_Migration {

	public function up()
	{
		//creating table for sub sub categories
			$this->db->query("
			CREATE TABLE devices
			(
			id int NOT NULL AUTO_INCREMENT,
			name varchar(255) NOT NULL,
			path varchar(255) NOT NULL,
			categories_id int DEFAULT 0,
			sub_categories_id int DEFAULT 0,
			sub_sub_categories_id int DEFAULT 0,
			PRIMARY KEY (ID),
			FOREIGN KEY (categories_id) REFERENCES categories(id),
			FOREIGN KEY (sub_categories_id) REFERENCES sub_categories(id),
			FOREIGN KEY (sub_sub_categories_id) REFERENCES sub_sub_categories(id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;
			");

	}

	public function down()
	{
		$this->db->query('DROP TABLE devices');
	}

}
