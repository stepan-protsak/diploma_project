<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_create_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	// creating new category
	function create_new_category($name)
	{
		$this->db->query("
			insert into categories (name)
			values (".$this->db->escape($name).")
		");
	}

	// creating new sub category
	function create_new_sub_category($name, $categories_id)
	{
		$this->db->query("
			insert into sub_categories (name, categories_id)
			values (".$this->db->escape($name).",".$this->db->escape($categories_id).")
		");
	}

	// creating new sub category
	function create_new_sub_sub_category($name, $sub_categories_id)
	{
		$this->db->query("
			insert into sub_sub_categories (name, sub_categories_id)
			values (".$this->db->escape($name).",".$this->db->escape($sub_categories_id).")
		");
	}
}