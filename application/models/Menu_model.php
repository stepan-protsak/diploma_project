<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	function select_menu_items()
	{
		//varibable to hold sorted result
		$sort = array();

		//varible to hold sorting flag
		$prev = '';
		
		/*------------------------------------------------------------------------------------------------------
			CATEGORIES QUERIES

			START
		*/
		// querry to know all sub categories 
		$query = $this->db->query("
			select categories.name as 'categories', sub_categories.name as 'sub_categories' 
			from categories 
			join sub_categories on categories.id = sub_categories.categories_id
			order by categories;
			");
		$result = $query->result_array();

		// querry to know all sub sub categories
		$query1 = $this->db->query("
			select categories.name as 'categories', sub_categories.name as 'sub_categories', sub_sub_categories.name as 'sub_sub_categories' 
			from categories
			join sub_categories on categories.id = sub_categories.categories_id
			join sub_sub_categories on sub_categories.id = sub_sub_categories.sub_categories_id
			order by categories;
			");
		$result1 = $query1->result_array();

		// querry to know all categories that dont have sub categories
		$query2 = $this->db->query("
			select categories.name as 'categories' 
			from categories 
			where id not in (select categories_id as 'id' from sub_categories)
			order by categories;
			");
		$result2 = $query2->result_array();
		
		/*
			CATEGORIES QUERIES

			END
		------------------------------------------------------------------------------------------------------*/


		/*------------------------------------------------------------------------------------------------------
			DEVICES QUERIES

			START
		*/
		// querry to get all the devices that have sub_sub_categories

		$query3 = $this->db->query("
			select devices.name, devices.path, categories.name as categories, sub_categories.name as sub_categories, sub_sub_categories.name as sub_sub_categories 
			from devices 
			join categories on categories.id = devices.categories_id 
			join sub_categories on sub_categories.id = devices.sub_categories_id 
			join sub_sub_categories on devices.sub_sub_categories_id = sub_sub_categories.id
			order by name;
			");
		$result3 = $query3->result_array();

		// querry to get all the devices that have only sub_categories

		$query4 = $this->db->query("
				select devices.name, devices.path, categories.name as categories, sub_categories.name as sub_categories
				from devices
				join categories on categories.id = devices.categories_id 
				join sub_categories on sub_categories.id = devices.sub_categories_id
				where sub_sub_categories_id = 0
				order by name;
			");
		$result4 = $query4->result_array();

		// querry to get aal the evices that have only categories
		$query5 = $this->db->query("
				select devices.name, devices.path, categories.name as categories
				from devices
				join categories on categories.id = devices.categories_id 
				where sub_sub_categories_id = 0 and sub_categories_id = 0
				order by name;
			");
		$result5 = $query5->result_array();

		/*
			DEVICES QUERIES

			END
		------------------------------------------------------------------------------------------------------*/


		/*------------------------------------------------------------------------------------------------------
			CATEGORIES SORTING

			START
		*/
		// sorting results of firs query to one array
		foreach ($result as $row) {
		
			if ($row['categories'] != $prev) 
			{
				$sort[$row['categories']] = array();
			}

			$sort[$row['categories']][$row['sub_categories']] = array() ;
			
			//array_push($sort[$row['categories']], $row['sub_categories']);

			$prev = $row['categories'];
		}

		// adding result of the second querry
		foreach ($result1 as $row) {
			$sort[$row['categories']][$row['sub_categories']][$row['sub_sub_categories']] = array();
		}

		// addinng result of the third querry
		foreach ($result2 as $row) {
			$sort[$row['categories']] = array();
		}

		/*
			CATEGORIES SORTING

			END
		------------------------------------------------------------------------------------------------------*/


		/*------------------------------------------------------------------------------------------------------
			DEVICES SORTING

			START
		*/		
		//adding to sort array devices that have sub sub catgories
		foreach ($result3 as $row) {
			$sort[$row['categories']][$row['sub_categories']][$row['sub_sub_categories']][$row['name']] = $row['path'];
		}

		//adding to sort array devices that have sub categories
		foreach ($result4 as $row) {
			$sort[$row['categories']][$row['sub_categories']][$row['name']] = $row['path'];
		}

		//adding to sort array devices that have categories
		foreach ($result5 as $row) {
			$sort[$row['categories']][$row['name']] = $row['path'];
		}
		/*
			DEVICES SORTING

			END
		------------------------------------------------------------------------------------------------------*/

		return $sort;
	}

}