<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_upload_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	// getting all categoriess
	function select_all_categories()
	{
		$query = $this->db->query("
			select id, name from categories
		");

		$result = $query->result_array();

		return $result;
	}

	// getting all sub categoriess
	function select_all_sub_categories()
	{
		$query = $this->db->query("
			select * from sub_categories
		");

		$result = $query->result_array();

		return $result;
	}

	// getting all sub categoriess
	function select_all_sub_sub_categories()
	{
		$query = $this->db->query("
			select * from sub_sub_categories
		");

		$result = $query->result_array();

		return $result;
	}

	// adding file to the database
	function add_device($name, $path, $categories_id, $sub_categories_id, $sub_sub_categories_id)
	{
		$this->db->query("
			insert into devices (name, path, categories_id, sub_categories_id, sub_sub_categories_id)
			values (".$this->db->escape($name).",
					".$this->db->escape($path).",
					".$this->db->escape($categories_id).",
					".$this->db->escape($sub_categories_id).",
					".$this->db->escape($sub_sub_categories_id).")
		");
	}
}