<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
	}

	// deleting device from the database and application
	function delete_device($path)
	{
		$this->db->query("
			delete from devices where path =".$this->db->escape($path)."
		");

		//deleting file from server
		unlink('files/'. $path);
	}

	// getting id of sub sub category
	function get_sub_sub_category_id($name)
	{
		$query = $this->db->query("
			select id from sub_sub_categories where name=".$this->db->escape($name)."
		");

		$id = $query->result_array();

		$id = $id[0]['id'];

		return $id;
	}

	// getting id of sub category
	function get_sub_category_id($name)
	{
		$query = $this->db->query("
			select id from sub_categories where name=".$this->db->escape($name)."
		");

		$id = $query->result_array();

		$id = $id[0]['id'];

		return $id;
	}

	// getting id of category
	function get_category_id($name)
	{
		$query = $this->db->query("
			select id from categories where name=".$this->db->escape($name)."
		");

		$id = $query->result_array();

		$id = $id[0]['id'];

		return $id;
	}

	// delete sub sub category and all files that 
	function delete_sub_sub_category($name)
	{
		// getting id of sub sub category that will be deleted
		$id = $this->get_sub_sub_category_id($name);
		
		$query = $this->db->query("
			select path from devices where sub_sub_categories_id = ".$this->db->escape($id)."
		");

		$result = $query->result_array();

		foreach ($result as $row) {
			$this->delete_device($row['path']);
		}

		$this->db->query("
			delete from sub_sub_categories where id =".$this->db->escape($id)."
		");
	}
	
	// delete sub category
	function delete_sub_category($name)
	{
		// getting id of sub_category
		$id = $this->get_sub_category_id($name);

		// getting sub sub categories that belongs to that sub category
		$query = $this->db->query("
			select name from sub_sub_categories where sub_categories_id =".$this->db->escape($id)."
		");

		$result = $query->result_array();
		
		// if result querry result not empty calling delete sub_sub_category method else deleting all the devices
		if (!empty($result))
		{
			foreach ($result as $row) {
				$this->delete_sub_sub_category($row['name']);
			}
		}
		else
		{
			// gettig all the decvices that belongs to sub category
			$query1 = $this->db->query("
				select path from devices where sub_categories_id =".$this->db->escape($id)." and sub_sub_categories_id = 0
			");

			$result1= $query1->result_array();

			// deleting each of them
			foreach ($result1 as $row) {
				$this->delete_device($row['path']);
			}
		}

		// and at last deleting sub category itself
		$this->db->query("
			delete from sub_categories where id =".$this->db->escape($id)."
		");
	}

	// function to delete category
	function delete_category($name)
	{
		// getting id of a category
		$id = $this->get_category_id($name);

		// getting all sub categories that belongs to this category
		$query = $this->db->query("
			select name from sub_categories where categories_id =".$this->db->escape($id)."
		");

		$result = $query->result_array();

		// if querry result are not empty then we call delete_sub_category else deleting all the devices
		if (!empty($result))
		{
			foreach ($result as $row) {
				$this->delete_sub_category($row['name']);
			}
		}
		else
		{
			// gettig all the decvices that belongs to sub category
			$query1 = $this->db->query("
				select path from devices where categories_id =".$this->db->escape($id)." and sub_sub_categories_id = 0 and sub_categories_id = 0
			");

			$result1= $query1->result_array();

			// deleting each of them
			foreach ($result1 as $row) {
				$this->delete_device($row['path']);
			}
		}

		// and at last deleting category from database
		$this->db->query("
			delete from categories where id =".$this->db->escape($id)."
		");
	}
}