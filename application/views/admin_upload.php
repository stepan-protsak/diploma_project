<!-- Navbar -->
<ul class="w3-navbar w3-light-blue w3-top w3-card-2 w3-left-align w3-large" style="z-index:4;">
  <li class="w3-right"><a class="w3-hover-white" href="Admin_home/logout" class="w3-theme-l1">Вихід</a></li>
  <li class="w3-hide-small"><a href="Admin_home" class="w3-hover-white">Довідка</a></li>
  <li class="w3-hide-small"><a href="Admin_create" class="w3-hover-white">Створити</a></li>
  <li class="w3-hide-small"><a href="Admin_upload" class="w3-white">Додати</a></li>
  <li class="w3-hide-small"><a href="Admin_delete" class="w3-hover-white">Видалити</a></li>
</ul>

  <div class="w3-row w3-padding-hor-64">
  <div class="w3-row">
    <div class="w3-full w3-container">
      <h1 class="w3-text-teal">Додати прилад</h1>

          <?php echo form_open_multipart('Admin_upload/do_upload');?>

          <p> Виберіть розділ в який ви хочете додати прилад:</p>
          <p>
            <select id="select1" name="categories_id">
              <?php
              $count = 0;
              foreach ($categories as $row) {
                if ($count == 0) {
                  $flag = $row['id'];
                }

                echo "<option value='".$row['id']."''>".$row['name']."</option>";

                $count +=1;
              }
              ?>
            </select>

            <select id="select2" name="sub_categories_id">
              <option value="0" class="hidden"></option>
              <?php
              foreach ($sub_categories as $row) {
                if (!($row['categories_id'] == $flag)){
                  echo "<option class='hidden' data-categories-id='".$row['categories_id']."' value='".$row['id']."''>".$row['name']."</option>";
                }
                else
                {
                  echo "<option data-categories-id='".$row['categories_id']."' value='".$row['id']."''>".$row['name']."</option>";

                }
              }
              ?>
            </select>

            <select id="select3" name="sub_sub_categories_id">
            <option value="0" class="hidden"></option>
              <?php
              foreach ($sub_sub_categories as $row) {
                echo "<option class='hidden' data-sub-categories-id='".$row['sub_categories_id']."' value='".$row['id']."''>".$row['name']."</option>";
              }
              ?>
            </select>
          </p>

          <p> Виберіть прилад який ви хочете додати (тільки .pdf формат):</p>
          <p><?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message');}?></p>

          <p><input type="file" name="userfile" size="20" /></p>

          <p>Наижміть кнопку загрузити коли будете готові:</p>
          <p><input type="submit" value="Загрузити" /></p>
    </div>
  </div>
</div>