<!-- Navbar -->
<ul class="w3-navbar w3-light-blue w3-top w3-card-2 w3-left-align w3-large" style="z-index:4;">
  <li class="w3-right"><a class="w3-hover-white" href="Admin_home/logout" class="w3-theme-l1">Вихід</a></li>
  <li class="w3-hide-small"><a href="Admin_home" class="w3-white">Довідка</a></li>
  <li class="w3-hide-small"><a href="Admin_create" class="w3-hover-white">Створити</a></li>
  <li class="w3-hide-small"><a href="Admin_upload" class="w3-hover-white">Додати</a></li>
  <li class="w3-hide-small"><a href="Admin_delete" class="w3-hover-white">Видалити</a></li>
</ul>

  <div class="w3-row w3-padding-hor-64">
  <div class="w3-row">
    <div class="w3-full w3-container">
      <h1 class="w3-text-teal">Панель адміністрації</h1>
      <h2>Ласкаво просимо до панелі адміністрації!</h2>
      <p>В розділі <span style="font-style: italic;"> створити </span> - ви можете створити новий розділ або підрозділ в базі даних. <span style="color:red;"> Будь ласка правильно вказуйте назву розділу чи підрозділу, не називайте розділ чи підрозділ однаковими чи вже існуючими іменами для запобігання конфліктів. </span></p>
      <p>В розділі <span style="font-style: italic;"> додати </span> - ви можете додати новий прилад до бази даних вказавши розділ і підрозділ до якого він відноситсься. <span style="color:red;"> Будьте уважні при виборі розділу і підрозділу, щоб ваш прилад опинився в подтрібному місці.</span></p>
      <p>В розділі <span style="font-style: italic;"> видалити </span> - ви можете видаляти з бази даних розділи, підрозділи чи прилади. <span style="color:red;"> Будьте обережні при видаленні розділів чи підрозділів, так як видаливши розділ чи підрозділ ви автоматично видаляєте всі прилади і підрозділи, що до нього відносяться.</span></p>
      <p>Натиснувши на <span style="font-style: italic;">вихід</span> ви повернетесь до головної сторінки бази даних.</p>      
    </div>
  </div>
</div>