<!-- Navbar -->
<ul class="w3-navbar w3-light-blue w3-top w3-card-2 w3-left-align w3-large" style="z-index:4;">
  <li class="w3-right"><a class="w3-hover-white" href="Admin_home/logout" class="w3-theme-l1">Вихід</a></li>
  <li class="w3-hide-small"><a href="Admin_home" class="w3-hover-white">Довідка</a></li>
  <li class="w3-hide-small"><a href="Admin_create" class="w3-hover-white">Створити</a></li>
  <li class="w3-hide-small"><a href="Admin_upload" class="w3-hover-white">Додати</a></li>
  <li class="w3-hide-small"><a href="Admin_delete" class="w3-white">Видалити</a></li>
</ul>

  <div class="w3-row w3-padding-hor-64">
  <div class="w3-row">
    <div class="w3-full w3-container">
      <?php
      $count = 1;
      foreach ($sort as $key => $value) {
      echo "<div class='w3-accordion'>";
        echo "<p class='first bold w3-padding w3-container w3-card-2'>";
        echo "<a onclick='myAccordion(".(string) $count.")' href='javascript:void(0)'>".$key." <i class='fa fa-caret-down'></i></a>";
        echo "<a  class='w3-btn w3-red w3-right' href='Admin_delete/delete_category/".$key."'> Видалити </a>";
        echo "</p>";
        echo "<div id='".(string) $count."' class='w3-accordion-content w3-animate-left'>";
        
        foreach ($value as $key => $value) {
          if (gettype($value) == "string") 
          {
            echo "<p class='second w3-padding w3-container w3-card-2'>";
            echo $key;
            echo "<a class='w3-btn w3-red w3-padding w3-right' href='Admin_delete/delete_device/".$value."'> Видалити </a>";
            echo "</p>";
          }
          else
          {
            $count += 1;
            //echo $count;
            echo "<div class='w3-accordion'>";
              echo "<p class='second bold w3-padding w3-container w3-card-2'>";
              echo "<a style='display:inline' class='w3-hover-none'onclick='myAccordion(".(string) $count.")' href='javascript:void(0)'>".$key." <i class='fa fa-caret-down'></i></a>";
              echo "<a  class='w3-btn w3-red w3-right' href='Admin_delete/delete_sub_category/".$key."'> Видалити </a>";
              echo "</p>";
              echo "<div id='".(string) $count."' class='w3-accordion-content w3-animate-left'>";

              foreach ($value as $key => $value) {
                if (gettype($value) == "string") 
                {
                  echo "<p class='third w3-padding w3-container w3-card-2'>";
                  echo $key;
                  echo "<a class='w3-btn w3-red w3-padding w3-right' href='Admin_delete/delete_device/".$value."'> Видалити </a>";
                  echo "</p>";
                }
                else
                {
                  $count += 1;
                  echo "<div class='w3-accordion'>";
                    echo "<p class='third bold w3-padding w3-container w3-card-2'>";
                    echo "<a style='display:inline' class='w3-hover-none' onclick='myAccordion(".(string) $count.")' href='javascript:void(0)'>".$key." <i class='fa fa-caret-down'></i></a>";
                    echo "<a  class='w3-btn w3-red w3-right' href='Admin_delete/delete_sub_sub_category/".$key."'> Видалити </a>";
                    echo "</p>";
                    echo "<div id='".(string) $count."' class='w3-accordion-content w3-animate-left'>";
                    foreach ($value as $key => $value) {
                      if (gettype($value) == "string") 
                      {
                        echo "<p class='last w3-padding w3-container w3-card-2'>";
                        echo $key;
                        echo "<a  class='w3-btn w3-red w3-padding w3-right' href='Admin_delete/delete_device/".$value."'> Видалити </a>";
                        echo "</p>";
                      }
                    }
                  echo "</div>";
                  echo "</div>";
                }
              }
            echo "</div>";
            echo "</div>";
          }
        }
        echo "</div>";
        echo "</div>";
        $count += 1;
      }

      ?>
    </div>
  </div>
</div>