<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->layout->printTitle();?>
		<?php echo $this->layout->printConfig();?>
		<?php echo $this->layout->printCss();?>
		<?php echo $this->layout->printJs();?>
		<?php echo $this->layout->printMeta();?>
	</head>
	<body class="w3-light-grey">
		<div class="container-fluid text-center">
			<div class="row content">
				<?php echo $this->layout->printPage();?>
			</div>
		</div>
	</body>
</html>