<!-- Navbar -->
<ul class="w3-navbar w3-light-blue w3-top w3-card-2 w3-left-align w3-large" style="z-index:4;">
  <li class="w3-right"><a class="w3-hover-white" href="Admin_home/logout" class="w3-theme-l1">Вихід</a></li>
  <li class="w3-hide-small"><a href="Admin_home" class="w3-hover-white">Довідка</a></li>
  <li class="w3-hide-small"><a href="Admin_create" class="w3-white">Створити</a></li>
  <li class="w3-hide-small"><a href="Admin_upload" class="w3-hover-white">Додати</a></li>
  <li class="w3-hide-small"><a href="Admin_delete" class="w3-hover-white">Видалити</a></li>
</ul>

  <div class="w3-row w3-padding-hor-64">
  <div class="w3-row">
    <div class="w3-full w3-container">
      <h1 class="w3-text-teal">Створити розділ або підрозділ</h1>
      
      <?php echo form_open_multipart('Admin_create/create_category');?>
        <fieldset style="width: 25%">
          <legend>Створити розділ:</legend>
          <p style="margin-left: 5%"> Назва:
          <input type="text" name="name" required></input>
          </p>
          <p><input style="margin-left: 5%" type="submit" value="Створити" /></p>
          <p><?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message');}?></p>
        </fieldset>
        </br>
      </form>
      
      <?php echo form_open_multipart('Admin_create/create_sub_category');?>
        <fieldset style="width: 35%">
          <legend>Створити підрозділ:</legend>
          <p style="margin-left: 5%; margin-right: 5%">
            Назва розділу:
            <select name="categories_id">
              <?php
                foreach ($categories as $row) {
                  echo "<option value='".$row['id']."''>".$row['name']."</option>";
                }
              ?>
            </select>
          </p>
          <p style="margin-left: 5%; margin-right: 5%">
            Назва підрозділу:
            <input type="text" name="name" required></input>
          </p>
          <p><input style="margin-left: 5%" type="submit" value="Створити" /></p>
          <p><?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message');}?></p>
        </fieldset>
      </form>
      </br>

      <?php echo form_open_multipart('Admin_create/create_sub_sub_category');?>
        <fieldset style="width: 35%">
          <legend>Створити підрозділ до підрозділа:</legend>

          <p style="margin-left: 5%; margin-right: 5%">
            Назва підрозділу:
            <select name="sub_categories_id">
              <?php
              foreach ($sub_categories as $row) {
                echo "<option value='".$row['id']."''>".$row['name']."</option>";
              }
              ?>
            </select>
          </p>

          <p style="margin-left: 5%; margin-right: 5%">
            Назва підрозділу:
            <input type="text" name="name" required></input>
          </p>
          <p><input style="margin-left: 5%" type="submit" value="Створити" /></p>
          <p><?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message');}?></p>
        </fieldset>
      </form>

    </div>
  </div>
</div>