<header class="w3-container w3-card-2 w3-light-blue">
  <h1>Вхід для адміністрації</h1>
</header>

<div class="w3-container w3-half w3-margin-top" style="position:absolute; top:32.5%; left:32.5%; width: 30%">

<?php echo validation_errors(); ?>
<?php echo form_open('Verifylogin', $attributes); ?>

<p>
<label for="username" class="w3-label w3-validate">Ім'я користувача:</label>
<input id="username" name="username" class="w3-input w3-light-gray" type="text" style="background:#f1f1f1;" required>
</p>

<p>
<label for="password" class="w3-label w3-validate">Пароль:</label>
<input type="password" size="20" id="passowrd" name="password" class="w3-input w3-light-gray" type="password" style="background:#f1f1f1;" required>
</p>

<p>
<button type="submit" value="Login" class="w3-btn w3-section w3-light-blue w3-ripple w3-right"> Вхід </button></p>

</form>

</div>