<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#session_start();
class Admin_create extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		$this->load->model('Admin_create_model');
	}

	public function index()
	{	
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
		
			// setting session daata
			$session_data = $this->session->userdata('logged_in');

			// Setting meta tag
			$this->layout->setMeta(array('name'=>'viewport','content'=>'width=device-width, initial-scale=1'));
		
			// Font awsome library
			#$this->layout->addCss('font-awesome-4.6.1/css/font-awesome.min', FALSE);
		
			// W3 schools library
			$this->layout->addCss('w3');
		
			// Main css file
			$this->layout->addCss('admin');

			// Delet style
			//$this->layout->addCss('delete');
		
			// Main javascript file 
			$this->layout->addJs('main');

			// Setting page title
			$this->layout->setTitle("Створити");

			// loading upload model
			$this->load->model('Admin_upload_model');

			// getting all categories from the select
			$data['categories'] = $this->Admin_upload_model->select_all_categories();

			// getting all sub categories for the second select
			$data['sub_categories'] = $this->Admin_upload_model->select_all_sub_categories();
		
			// Loading page
			$this->layout->page('admin_create', $data,  Layout::LAYOUT_TYPE_NO_SIDEBARS);

		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}

	}

	public function create_category()
	{
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{

			// getting categories id from user
			$name = $this->input->post('name');

			// adding new category to the database
			$this->Admin_create_model->create_new_category($name);

			// sending succes message
			$data['message'] = "Розділ успішно створено";
			$this->session->set_flashdata('message', $data['message']);

			redirect('Admin_create');
		
		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}

	}

	public function create_sub_category()
	{
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{

			// getting categories id from user
			$name = $this->input->post('name');
			$categories_id = $this->input->post('categories_id');
			
			$this->Admin_create_model->create_new_sub_category($name, $categories_id);

			// sending succes message
			$data['message'] = "Підрозділ успішно створено";
			$this->session->set_flashdata('message', $data['message']);

			redirect('Admin_create');

		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}
	}

	public function create_sub_sub_category()
	{
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
		
			// getting categories id from user
			$name = $this->input->post('name');
			$sub_categories_id = $this->input->post('sub_categories_id');
			
			$this->Admin_create_model->create_new_sub_sub_category($name, $sub_categories_id);

			// sending succes message
			$data['message'] = "Підрозділ успішно створено";
			$this->session->set_flashdata('message', $data['message']);

			redirect('Admin_create');

		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}
	}

}	