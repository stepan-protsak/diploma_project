<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#session_start();
class Admin_upload extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
	}	

	public function index()
	{	
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
		
			// setting session daata
			$session_data = $this->session->userdata('logged_in');

			// Setting meta tag
			$this->layout->setMeta(array('name'=>'viewport','content'=>'width=device-width, initial-scale=1'));
		
			// Font awsome library
			#$this->layout->addCss('font-awesome-4.6.1/css/font-awesome.min', FALSE);
		
			// W3 schools library
			$this->layout->addCss('w3');
		
			// Main css file
			$this->layout->addCss('admin');

			// Main javascript file 
			$this->layout->addJs('main');

			// Loading jQuery library
			$this->layout->addJs('jquery-2.2.0.min');

			// Rebuild chart script 
			$this->layout->addJs('upload');

			// Setting page title
			$this->layout->setTitle("Додати");
			
			// loading upload model
			$this->load->model('Admin_upload_model');

			// getting all categories from the select
			$data['categories'] = $this->Admin_upload_model->select_all_categories();

			// getting all sub categories for the second select
			$data['sub_categories'] = $this->Admin_upload_model->select_all_sub_categories();

			// getting sub sub categories for the third select
			$data['sub_sub_categories'] = $this->Admin_upload_model->select_all_sub_sub_categories();

			// Loading page
			$this->layout->page('admin_upload', $data,  Layout::LAYOUT_TYPE_NO_SIDEBARS);

		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}

	}

	// function that handles uploads
	public function do_upload()
	{

		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
		
			//defining upload path
			$config['upload_path'] = './files';
			//setting allowed file types
			$config['allowed_types'] = 'pdf';

			//loading upload library with our configuration
			$this->load->library('upload', $config);

			// getting categories id from user
			$categories_id = $this->input->post('categories_id');

			// getting sub_categories id from user
			$sub_categories_id = $this->input->post('sub_categories_id');

			// getting sub_sub_categories id from user
			$sub_sub_categories_id = $this->input->post('sub_sub_categories_id');

			//if upload failed display error message
			if (!$this->upload->do_upload('userfile'))
			{
				$data['message'] = $this->upload->display_errors();
				$this->session->set_flashdata('message', $data['message']);
				redirect('Admin_upload');
			}
			else
			{
				//getting data of uploaded file
				$data = $this->upload->data();
				
				// loading upload model
				$this->load->model('Admin_upload_model');

				// getting full name of the file
				$path = $data['file_name'];

				// getting just name of the file
				$name = $data['raw_name'];

				// adding file to database
				$this->Admin_upload_model->add_device($name, $path, $categories_id, $sub_categories_id, $sub_sub_categories_id);

				$data['message'] = "Файл успішно завантажено";
				$this->session->set_flashdata('message', $data['message']);

				//loading index
				redirect('Admin_upload');
			}

		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}

	}

}