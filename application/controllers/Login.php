<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{	
			//loading all needed helpers
			$this->load->helper('url');
			$this->load->helper('form');

			// Setting meta tag
			$this->layout->setMeta(array('name'=>'viewport','content'=>'width=device-width, initial-scale=1'));
			
			// Font awsome library
			#$this->layout->addCss('font-awesome-4.6.1/css/font-awesome.min', FALSE);
			
			// W3 schools library
			$this->layout->addCss('w3');

			// Setting page title
			$this->layout->setTitle("Вхід");
			$data['attributes'] = 'class = "w3-container w3-card-4"';

			// Loading page
			$this->layout->page('log_in', $data, Layout::LAYOUT_TYPE_NO_SIDEBARS);
	}
}