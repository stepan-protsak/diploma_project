<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#session_start();
class Admin_delete extends CI_Controller {

	public function index()
	{	
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
		
			// setting session daata
			$session_data = $this->session->userdata('logged_in');
			
			// loading menu model
			$this->load->model('Menu_model');

			// getting mennu items from database
			$data['sort'] = $this->Menu_model->select_menu_items();

			// Setting meta tag
			$this->layout->setMeta(array('name'=>'viewport','content'=>'width=device-width, initial-scale=1'));
		
			// Font awsome library
			$this->layout->addCss('font-awesome-4.6.1/css/font-awesome.min', FALSE);
		
			// W3 schools library
			$this->layout->addCss('w3');
		
			// Main css file
			$this->layout->addCss('admin');

			// Delet style
			$this->layout->addCss('delete');
		
			// Main javascript file 
			$this->layout->addJs('main');

			// Setting page title
			$this->layout->setTitle("Видалити");
		
			// Loading page
			$this->layout->page('admin_delete', $data,  Layout::LAYOUT_TYPE_NO_SIDEBARS);

		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}

	}

	// function to delete files
	public function delete_device($path)
	{
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
			// loading model
			$this->load->model('Admin_model');
			
			// getting model function to delete file from db
			$this->Admin_model->delete_device($path);
			
			//loading index page
			redirect('Admin_delete');
		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}
	}

	// function to delete sub sub categories 
	public function delete_sub_sub_category($name)
	{
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{

			$name = urldecode($name);
			// loading model
			$this->load->model('Admin_model');
		
			$this->Admin_model->delete_sub_sub_category($name);

			//loading index page
			redirect('Admin_delete');
		
		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}
	}

	// funtion to delete sub categories
	public function delete_sub_category($name)
	{
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{

			$name = urldecode($name);
			
			// loading model
			$this->load->model('Admin_model');
		
			$this->Admin_model->delete_sub_category($name);

			//loading index page
			redirect('Admin_delete');

		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}
	}

	// function to delete category
	public function delete_category($name)
	{
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
		
			$name = urldecode($name);
			
			// loading model
			$this->load->model('Admin_model');
		
			$this->Admin_model->delete_category($name);

			//loading index page
			redirect('Admin_delete');
		}
		else
		{
		//If no session, redirect to login page
		redirect('login', 'refresh');
		}
	}
}