<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{	
		// Loading helpers and models
		$this->load->model('Menu_model');
		$this->load->helper('url');

		// Setting meta tag
		$this->layout->setMeta(array('name'=>'viewport','content'=>'width=device-width, initial-scale=1'));
		
		// Font awsome library
		$this->layout->addCss('font-awesome-4.6.1/css/font-awesome.min');
		
		// W3 schools library
		$this->layout->addCss('w3');
		
		// Maint css file
		$this->layout->addCss('standart');
		
		// Main javascript file 
		$this->layout->addJs('main');

		// Setting page title
		$this->layout->setTitle("Головна");

		// Getting menu items from model
		$data['sort'] = $this->Menu_model->select_menu_items();
		
		// Loading page
		$this->layout->page('welcome_message', $data);
	}
}
