<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#session_start();
class Admin_home extends CI_Controller {

	public function index()
	{	
		//Cheking if user is logged in
		if($this->session->userdata('logged_in'))
		{
		
			// setting session daata
			$session_data = $this->session->userdata('logged_in');

			// Setting meta tag
			$this->layout->setMeta(array('name'=>'viewport','content'=>'width=device-width, initial-scale=1'));
		
			// Font awsome library
			#$this->layout->addCss('font-awesome-4.6.1/css/font-awesome.min', FALSE);
		
			// W3 schools library
			$this->layout->addCss('w3');
		
			// Maint css file
			$this->layout->addCss('admin');
		
			// Main javascript file 
			$this->layout->addJs('main');

			// Setting page title
			$this->layout->setTitle("Довідка");
		
			// Loading page
			$this->layout->page('admin_home', array(),  Layout::LAYOUT_TYPE_NO_SIDEBARS);

		}
		else
		{
		//If no session, redirect to login page
     	redirect('login', 'refresh');
		}

	}

	// function to log out from session
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('Welcome', 'refresh');
	}
}