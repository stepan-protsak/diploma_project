<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Library to for layout!
*/

class Layout {

	//const to hold type of layout
	const LAYOUT_TYPE_LEFT_SIDEBAR = 1;

	//const to hold type of layout
	const LAYOUT_TYPE_RGHT_SIDEBAR = 2;

	//const to hold type of layout
	const LAYOUT_TYPE_NO_SIDEBARS = 3;

	//const to hold type of layout
	const LAYOUT_TYPE_NO_FOOTER = 4;

	//hold template type
	private $templateType = array();
	
	//hold meta tags information
	private $metaTags = array();
	
	//hold config array for autoload
	private $items; 

	//hold CI instance
	private $CI;

	//hold title
	private $title;
	
	//hold css includes
	private $cssFiles = array();

	//hold js includes
	private $jsFiles = array();

	//just reference to Codeigniter instance
	public function __construct()
	{
		$this->CI =& get_instance();
	}

	//Method to set title
	//@param string $title - title name
	public function setTitle($titleInfo)
	{
		$this->title = '<title>' . $titleInfo . '</title>';
	}
	
	//Method to print title
	public function printTitle()
	{
		$finalTitle = '';
		return $finalTitle .= $this->title;
	}

	//Method to set meta tags
	//generates meta tag 
	//@param array $information - key are attribute name, and value is atribute content
	//for example (array('name' => 'robots', 'content' => 'no-cache'))
	// Generates: <meta name="robots" content="no-cache" />
	public function setMeta($information)
	{
		$this->CI->load->helper('html');
		$this->metaTags[] = meta($information); 
	}

	//Method to print meta tags
	public function printMeta()
	{
			$finalMeta = '';

			foreach ($this->metaTags as $tag) 
			{
				$finalMeta .= $tag;
			}
			return $finalMeta;
	}

	//Method for attaching CSS
	//@param string $fileName - CSS file name
	//css file must be at 'application/css/' folder
	public function addCss($fileName, $inc = TRUE)
	{
		$this->CI->load->helper('url');
		if ($inc == FALSE) 
		{
			$this->cssFiles[] = '<link rel="stylesheet" href="'. base_url() .'assets/css/'. $fileName .'.css">';
		}
		else
		{
			$this->cssFiles[] = '<link rel="stylesheet" type="text/css" href="'. base_url() .'assets/css/'. $fileName .'.css">';
		}
	}

	//Method to print css files in view
	public function printCss()
	{
			$finalCss = '';

			foreach ($this->cssFiles as $css) 
			{
				$finalCss .= $css;
			}
			return $finalCss;
	}

	//Method for attaching JS
	//@param string $fileName - JS file name
	//css file must be at 'application/js/' folder
	public function addJs($fileName)
	{
		$this->CI->load->helper('url');
		$this->jsFiles[] = '<script src="'. base_url() .'assets/js/'. $fileName .'.js"> </script>';
	}

	//Method to add external js
	//@param string $src - source of file
	public function addExteranalJs($src)
	{
		$this->jsFiles[] = '<script type="text/javascript" src="'.$src.'"></script>';
	}
	
	//Method to print js files in view
	public function printJs()
	{
			$finalJs = '';

			foreach ($this->jsFiles as $js) 
			{
				$finalJs .= $js;
			}
			return $finalJs;
	}

	//Method to print files that needed to be autoloaded
	//all files that needs to be autoloaded has to be at 'autoload array' in 'autoload' config 
	public function printConfig()
	{
		$includes = '';
		$items = $this->CI->config->item('autoload');
		foreach ($items as $include) {
			if (preg_match('/css$/', $include))
			{
				$includes .= '<link rel="stylesheet" href="'. $include .'">';
			}
			elseif (preg_match('/js$/', $include)) 
			{
				$includes .= '<script src="'. base_url() .'assets/js/'. $include .'"> </script>';
			}
		}
		return $includes;
	}


	//Method to print view to the page
	public function printPage()
	{
		$viewParts = '';
		foreach ($this->templateType as $part) {
			$viewParts .= $part;
		}
		return $viewParts;
	}

	//Method to load views
	//@param string $contentViewFile - name of the view file for the content section
	//@param array $viewData - data you want to send to the view
	//@param string $layoutType - type of layout you want to use, takes five params:
	// 1 - 'default' - default layout with all parts of page,
	// 2 - Layout::LAYOUT_TYPE_RGHT_SIDEBAR - loads page without lef sidebar,
	// 3 - Layout::LAYOUT_TYPE_LEFT_SIDEBAR - loads page without right sidebar,
	// 4 - Layout::LAYOUT_TYPE_NO_SIDEBARS - loads page without any sidebar,
	// 5 - Layout::LAYOUT_TYPE_NO_FOOTER - loads page without footer.
	public function page($contentViewFile, $viewData = array(), $layoutType = 'default') 
	{	
		// view types
		if ($layoutType == 'default')
		{
			$this->templateType[] = $this->CI->load->view('layouts/header', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/left_sidebar', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view($contentViewFile, $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/right_sidebar', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/footer', $viewData, TRUE);
			$this->CI->load->view('standart.php', $this->templateType);
		}
		elseif ($layoutType == Layout::LAYOUT_TYPE_RGHT_SIDEBAR)	
		{
			$this->templateType[] = $this->CI->load->view('layouts/header', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view($contentViewFile, $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/right_sidebar', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/footer', $viewData, TRUE);
			$this->CI->load->view('standart.php', $this->templateType);
		}
		elseif ($layoutType == Layout::LAYOUT_TYPE_LEFT_SIDEBAR) 
		{
			$this->templateType[] = $this->CI->load->view('layouts/header', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/left_sidebar', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view($contentViewFile, $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/footer', $viewData, TRUE);
			$this->CI->load->view('standart.php', $this->templateType);
		}
		elseif ($layoutType == Layout::LAYOUT_TYPE_NO_SIDEBARS) 
		{
			$this->templateType[] = $this->CI->load->view('layouts/header', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view($contentViewFile, $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/footer', $viewData, TRUE);
			$this->CI->load->view('standart.php', $this->templateType);
		}
		elseif ($layoutType == Layout::LAYOUT_TYPE_NO_FOOTER) 
		{
			$this->templateType[] = $this->CI->load->view('layouts/header', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/left_sidebar', $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view($contentViewFile, $viewData, TRUE);
			$this->templateType[] = $this->CI->load->view('layouts/right_sidebar', $viewData, TRUE);
			$this->CI->load->view('standart.php', $this->templateType);
		}
		else {
			echo "Sorry some kind of error!";
		}
	}
}